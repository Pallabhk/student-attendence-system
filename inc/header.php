<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Student Roll call System</title>

	<!--Bootstrap css-->
	<link rel="stylesheet" href="inc/css/bootstrap.min.css">
	<!--Main css-->
	<link rel="stylesheet" href="inc/css/style.css">
	<!--Bootstrap js -->
	<script type="text/javascript" src="inc/js/bootstrap.min.js"></script>
	<!--Jquery File-->
	<script type="text/javascript" src="inc/js/jquery-3.2.1.js"></script>
</head>
<body>

	<div class="container">
		<div class="well text-center">
			<h2>Student attendance system</h2>
		</div>