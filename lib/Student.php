<?php

	$filepath = realpath(dirname(__FILE__));
	include_once($filepath.'/database.php');
?>

<?php

	class Student{
		
		private $db;

		public function __construct(){
			$this->db = new Database();
		}
		public function getStudents(){
			$query = "select * from tbl_student";
			$result=$this->db->select($query);
			return $result;
		}
		public function insertStudent($name ,$roll){

			$name = mysqli_real_escape_string($this->db->link, $name);
			$roll = mysqli_real_escape_string($this->db->link, $roll);

			if(empty($name) || empty($roll)){
				$msg ="<div class='alert alert-danger'><strong>Error !</strong>Field must not be Empty</div>";
				return $msg;
			}else{
				$stu_query ="insert into tbl_student(name,roll) values('$name','$roll')";
				$stu_insert= $this->db->insert($stu_query);

				$att_query ="insert into tbl_attend(roll) values('$roll')";
				$stu_insert= $this->db->insert($att_query);

				if($stu_insert){
				$msg ="<div class='alert alert-success'><strong>Success !</strong>Data Inserted Successfully</div>";
				return $msg;
				}else{
					$msg ="<div class='alert alert-danger'><strong>Fail !</strong>Data not Inserted </div>";
				return $msg;
				}
			}
		}
	}
?>