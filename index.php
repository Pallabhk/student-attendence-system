<?php 
	include 'inc/header.php';
	include 'lib/Student.php';
?>

<?php
	$stu = new Student();
	$cur_date=date('Y-m-d');
?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>
					<a class="btn btn-success" href="add.php">Add Student</a>
					<a class="btn btn-info pull-right" href="">View all Student</a>
				</h2>
			</div>

			<div class="panel-body">
				<form action="" method="post">
					<div class="well text-center">
						<h2><strong>Date:</strong><?php  echo $cur_date;?></h2>
					</div>
					<table class="table table-striped">
						<tr>
							<th width="25%">Serial</th>
							<th width="25%">Student Name</th>
							<th width="25%">Student Roll</th>
							<th width="25%">Attendence</th>
						</tr>
						<?php
							$get_student =$stu->getStudents();
							if($get_student){
								$i =0;
								while ($value= $get_student->fetch_assoc()) {
									$i++;?>
						<tr>
							<td><?php echo $i;?></td>
							<td><?php echo $value['name'];?></td>
							<td><?php echo $value['roll'];?></td>
							<td>
								<input type="radio" name="attend[<?php echo $value['name'];?>]" value="present">P
								<input type="radio" name="attend[<?php echo $value['name'];?>]" value="absent">A
							</td>
						</tr>
						
								<?php }
							}?>
						
						<tr>
							<td>
								<input type="submit" class="btn btn-primary" name="submit" value="submit">
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	<?php include 'inc/footer.php';?>